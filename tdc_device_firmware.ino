// Connections:
//
// TDC7200      Pro-Mini
//  1 Enable      4
//  2 Trigg       -
//  3 Start       3
//  4 Stop        5
//  5 Clock       8            8Mhz clock output configured in Pro Mini Fuse settings
//  6 Nc          -
//  7 Gnd         Gnd
//  8 Intb        2
//  9 Dout        11
// 10 Din         12
// 11 Csb         10
// 12 Sclk        13
// 13 Vreg        -           Via 1uF to Gnd
// 14 Vdd         Vcc 3.3V        

#include "TDC7200.h"

#define PIN_TDC7200_INT       (9)
#define PIN_TDC7200_ENABLE    (7)

#define PIN_TDC7200_SPI_CS    (10)
#define TDC7200_CLOCK_FREQ_HZ (12000000)

#define HIST_SIZE 5600
#define CALIB_SIZE HIST_SIZE*4

static TDC7200 tof(PIN_TDC7200_ENABLE, PIN_TDC7200_SPI_CS, TDC7200_CLOCK_FREQ_HZ);
void read_measurement(void);
volatile uint32_t histogram[HIST_SIZE] = {0};
uint8_t calib[CALIB_SIZE] = {0};
volatile bool is_running = false;

void get_calibration() {
    for(int i = 0; i < CALIB_SIZE; i++) {
        uint8_t cal = Serial.read();
        calib[i] = cal;
    }
}

void setup()
{
    /* attachInterrupt(digitalPinToInterrupt(PIN_TDC7200_INT), read_measurement, LOW); */
    Serial.begin(115200);
    Serial.println("-- Starting TDC7200 test --");
    while (not tof.begin())
    {
        Serial.println("Failed to init TDC7200");
        delay(1000);
    }

    pinMode(PIN_TDC7200_INT, INPUT_PULLUP);     // active low (open drain)

    if (not tof.setupMeasurement( 20,         // cal2Periods
                                  1,          // avgCycles
                                  1,          // numStops
                                  1 ))        // mode
    {
        Serial.println(F("Failed to setup measurement"));
        while (1);
    }

    tof.setupOverflow(4500000ull);
    tof.startMeasurement();

}

static void ui64toa(uint64_t v, char * buf, uint8_t base)
{
  int idx = 0;
  uint64_t w = 0;
  while (v > 0)
  {
    w = v / base;
    buf[idx++] = (v - w * base) + '0';
    v = w;
  }
  buf[idx] = 0;
  // reverse char array
  for (int i = 0, j = idx - 1; i < idx / 2; i++, j--)
  {
    char c = buf[i];
    buf[i] = buf[j];
    buf[j] = c;
  }
}


void loop()
{   
    char c = Serial.read();
    switch (c) {
        case 'a':
            for(int i = 0; i < HIST_SIZE; i++) {
               for(int j = 0; j < 4; j++) {
                   Serial.write((histogram[i] >> 8*j) & 255);
               }
            }
           break;
        case 'c':
            for(int i = 0; i < HIST_SIZE; i++) {
                histogram[i] = 0;
            }
            break;
        case 'p':
            detachInterrupt(digitalPinToInterrupt(PIN_TDC7200_INT));
            break;
        case 't':
            attachInterrupt(digitalPinToInterrupt(PIN_TDC7200_INT), read_measurement, LOW);
            break;
        default:
            /* Serial.println(c); */
            break;
    }
}

void read_measurement(void) {
    uint64_t time;
    uint64_t lsb;
    /* int32_t rnd = random(-5, 6); */
    if (tof.readMeasurement(time, lsb))
    {
        time = (int)(time/54);
        if (time < HIST_SIZE) {
            histogram[time] += 1;
        }
    }
    tof.startMeasurement();
}
